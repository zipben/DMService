  
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataModels.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Models.CampaignModels
{
	[XmlRoot(ElementName="imageData")]
	public class ImageData {
		[Key]
		public int Id { get; set; }

		[XmlElement(ElementName="uniqueID")]
		public string UniqueID { get; set; }

		public Byte[] ImageContent { get; set; }
	}

	// [XmlRoot(ElementName="pc")]
	// public class Pc {
	// 	[Key]
	// 	public int Id { get; set; }

	// 	[XmlElement(ElementName="uid")]
	// 	public string Uid { get; set; }

	// 	[XmlElement(ElementName="label")]
	// 	public string Label { get; set; }

	// 	[XmlElement(ElementName="name")]
	// 	public string Name { get; set; }

	// 	[XmlElement(ElementName="ac")]
	// 	public string Ac { get; set; }

	// 	[XmlElement(ElementName="abilities")]
	// 	public string Abilities { get; set; }

	// 	[XmlElement(ElementName="hpMax")]
	// 	public string HpMax { get; set; }

	// 	[XmlElement(ElementName="hpCurrent")]
	// 	public string HpCurrent { get; set; }

	// 	[XmlElement(ElementName="hd")]
	// 	public string Hd { get; set; }

	// 	[XmlElement(ElementName="speed")]
	// 	public string Speed { get; set; }

	// 	[XmlElement(ElementName="init")]
	// 	public string Init { get; set; }

	// 	[XmlElement(ElementName="cr")]
	// 	public string Cr { get; set; }

	// 	[XmlElement(ElementName="savingThrow")]
	// 	public List<SavingThrow> SavingThrows { get; set; }

	// 	[XmlElement(ElementName="skill")]
	// 	public List<Skill> Skills { get; set; }

	// 	[XmlElement(ElementName="passive")]
	// 	public string Passive { get; set; }

	// 	[XmlElement(ElementName="action")]
	// 	public List<Action> Actions { get; set; }

	// 	[XmlElement(ElementName="languages")]
	// 	public string Languages { get; set; }
	// }

	[XmlRoot(ElementName="savingThrow")]
	public class SavingThrow {
		[Key]
		public int Id { get; set; }

		[XmlElement(ElementName="ability")]
		public string Ability { get; set; }

		[XmlElement(ElementName="modifier")]
		public string Modifier { get; set; }
	}

	[XmlRoot(ElementName="skill")]
	public class Skill {
		[Key]
		public int Id { get; set; }

		[XmlElement(ElementName="modifier")]
		public string Modifier { get; set; }


		[XmlElement(ElementName="id")]
		public SkillType Type { get; set; }
	}

	[XmlRoot(ElementName="attack")]
	public class Attack {
		[Key]
		public int Id { get; set; }

		[XmlElement(ElementName="atk")]
		public string Atk { get; set; }

		[XmlElement(ElementName="dmg")]
		public string Dmg { get; set; }

		[XmlElement(ElementName="name")]
		public string Name { get; set; }
	}

	[XmlRoot(ElementName="action")]
	public class Action {
		[Key]
		public int Id { get; set; }

		[XmlElement(ElementName="name")]
		public string Name { get; set; }

		[XmlElement(ElementName="text")]
		public string Text { get; set; }

		[XmlElement(ElementName="attack")]
		public Attack Attack { get; set; }

		[XmlElement(ElementName="recharge")]
		public string Recharge { get; set; }
	}

	// [XmlRoot(ElementName="npc")]
	// public class Npc {
	// 	[Key]
	// 	public int Id { get; set; }

	// 	[XmlElement(ElementName="uid")]
	// 	public string Uid { get; set; }

	// 	[XmlElement(ElementName="label")]
	// 	public string Label { get; set; }

	// 	[XmlElement(ElementName="name")]
	// 	public string Name { get; set; }

	// 	[XmlElement(ElementName="type")]
	// 	public string Type { get; set; }

	// 	[XmlElement(ElementName="alignment")]
	// 	public string Alignment { get; set; }

	// 	[XmlElement(ElementName="abilities")]
	// 	public string Abilities { get; set; }

	// 	[XmlElement(ElementName="hpMax")]
	// 	public string HpMax { get; set; }

	// 	[XmlElement(ElementName="hpCurrent")]
	// 	public string HpCurrent { get; set; }

	// 	[XmlElement(ElementName="hd")]
	// 	public string Hd { get; set; }

	// 	[XmlElement(ElementName="speed")]
	// 	public string Speed { get; set; }

	// 	[XmlElement(ElementName="action")]
	// 	public List<Action> Actions { get; set; }

	// 	[XmlElement(ElementName="enemy")]
	// 	public string Enemy { get; set; }

	// 	[XmlElement(ElementName="ac")]
	// 	public string Ac { get; set; }

	// 	[XmlElement(ElementName="savingThrow")]
	// 	public SavingThrow SavingThrow { get; set; }

	// 	[XmlElement(ElementName="skill")]
	// 	public List<Skill> Skills { get; set; }

	// 	[XmlElement(ElementName="passive")]
	// 	public string Passive { get; set; }

	// 	[XmlElement(ElementName="languages")]
	// 	public string Languages { get; set; }

	// 	[XmlElement(ElementName="cr")]
	// 	public string Cr { get; set; }

	// 	[XmlElement(ElementName="text")]
	// 	public string Text { get; set; }
	// }

	
	public class Creature {
		[Key]
		public int Id { get; set; }

		public CreatureCategory CreatureCategory { get; set; }

		[XmlElement(ElementName="uid")]
		public string Uid { get; set; }

		[XmlElement(ElementName="enemy")]
		public bool Enemy { get; set; }

		[XmlElement(ElementName="label")]
		public string Label { get; set; }

		[XmlElement(ElementName="name")]
		public string Name { get; set; }

		[XmlElement(ElementName="type")]
		public string Type { get; set; }

		[XmlElement(ElementName="alignment")]
		public string Alignment { get; set; }

		[XmlElement(ElementName="ac")]
		public string Ac { get; set; }

		[XmlElement(ElementName="armor")]
		public string Armor { get; set; }

		[XmlElement(ElementName="abilities")]
		public string Abilities { get; set; }

		[XmlElement(ElementName="hpMax")]
		public string HpMax { get; set; }

		[XmlElement(ElementName="hpCurrent")]
		public string HpCurrent { get; set; }

		[XmlElement(ElementName="hd")]
		public string Hd { get; set; }

		[XmlElement(ElementName="speed")]
		public string Speed { get; set; }

		[XmlElement(ElementName="skill")]
		public List<Skill> Skills { get; set; }

		[XmlElement(ElementName="passive")]
		public string Passive { get; set; }

		[XmlElement(ElementName="languages")]
		public string Languages { get; set; }

		[XmlElement(ElementName="cr")]
		public string Cr { get; set; }

		[XmlElement(ElementName="action")]
		public List<Action> Actions { get; set; }

		[XmlElement(ElementName="size")]
		public string Size { get; set; }

		[XmlElement(ElementName="senses")]
		public string Senses { get; set; }

		[XmlElement(ElementName="trait")]
		public List<Trait> Trait { get; set; }

		[XmlElement(ElementName="savingThrow")]
		public List<SavingThrow> SavingThrows { get; set; }

		[XmlElement(ElementName="immune")]
		public string Immune { get; set; }

		[XmlElement(ElementName="conditionImmune")]
		public string ConditionImmune { get; set; }
		
		[XmlElement(ElementName="reaction")]
		public Reaction Reaction { get; set; }

		[XmlElement(ElementName="spell")]
		public List<Spell> Spells { get; set; }

		[XmlElement(ElementName="slots")]
		public string Slots { get; set; }

		[XmlElement(ElementName="slotsCurrent")]
		public string SlotsCurrent { get; set; }

		[XmlElement(ElementName="resist")]
		public string Resist { get; set; }

		[XmlElement(ElementName="init")]
		public string Init { get; set; }

	}

	[XmlRoot(ElementName="combatant")]
	public class Combatant {
		[Key]
		public int Id { get; set; }
		[XmlElement(ElementName="initiative")]
		public string Initiative { get; set; }
		[XmlElement(ElementName="monster")]
		public Creature Creature { get; set; }
	}

	[XmlRoot(ElementName="encounter")]
	public class Encounter {
		[Key]
		public int Id { get; set; }
		[XmlElement(ElementName="name")]
		public string Name { get; set; }

		// [JsonProperty("state")]
		// [JsonConverter(typeof(StringEnumConverter))]
		[XmlElement(ElementName="state")]
		public EncounterState State { get; set; }
		[XmlElement(ElementName="current")]
		public string ActiveCombatantIndex { get; set; }
		[XmlElement(ElementName="round")]
		public string Round { get; set; }
		[XmlElement(ElementName="combatant")]
		public List<Combatant> Combatant { get; set; }
		[XmlElement(ElementName="note")]
		public Note Note { get; set; }
	}

	[XmlRoot(ElementName="trait")]
	public class Trait {
		[Key]
		public int Id { get; set; }
		[XmlElement(ElementName="name")]
		public string Name { get; set; }
		[XmlElement(ElementName="text")]
		public string Text { get; set; }
		[XmlElement(ElementName="attack")]
		public Attack Attack { get; set; }
	}

	[XmlRoot(ElementName="note")]
	public class Note {
		[Key]
		public int Id { get; set; }
		[XmlElement(ElementName="name")]
		public string Name { get; set; }
		[XmlElement(ElementName="text")]
		public string Text { get; set; }
		[XmlElement(ElementName="expanded")]
		public string Expanded { get; set; }
		[XmlElement(ElementName="imageData")]
		public ImageData ImageData { get; set; }
	}

	[XmlRoot(ElementName="adventure")]
	public class Adventure {
		[Key]
		public int Id { get; set; }

		[XmlElement(ElementName="imageData")]
		public ImageData ImageData { get; set; }

		[XmlElement(ElementName="name")]
		public string Name { get; set; }

		[XmlElement(ElementName="encounter")]
		public List<Encounter> Encounter { get; set; }

		[XmlElement(ElementName="note")]
		public List<Note> Note { get; set; }

		[XmlElement(ElementName="npc")]
		public List<Creature> Npcs { get; set; }
	}

	[XmlRoot(ElementName="reaction")]
	public class Reaction {
		[Key]
		public int Id { get; set; }
		[XmlElement(ElementName="name")]
		public string Name { get; set; }
		[XmlElement(ElementName="text")]
		public string Text { get; set; }
		[XmlElement(ElementName="attack")]
		public Attack Attack { get; set; }
	}

	[XmlRoot(ElementName="spell")]
	public class Spell {
		[Key]
		public int Id { get; set; }
		[XmlElement(ElementName="source")]
		public string Source { get; set; }
		[XmlElement(ElementName="name")]
		public string Name { get; set; }
		[XmlElement(ElementName="school")]
		public string School { get; set; }
		[XmlElement(ElementName="time")]
		public string Time { get; set; }
		[XmlElement(ElementName="range")]
		public string Range { get; set; }
		[XmlElement(ElementName="v")]
		public string V { get; set; }
		[XmlElement(ElementName="s")]
		public string S { get; set; }
		[XmlElement(ElementName="duration")]
		public string Duration { get; set; }
		[XmlElement(ElementName="text")]
		public string Text { get; set; }
		[XmlElement(ElementName="sclass")]
		public List<string> Sclass { get; set; }
		[XmlElement(ElementName="level")]
		public string Level { get; set; }
		[XmlElement(ElementName="m")]
		public string M { get; set; }
		[XmlElement(ElementName="materials")]
		public string Materials { get; set; }
		[XmlElement(ElementName="ritual")]
		public string Ritual { get; set; }
	}

	[XmlRoot(ElementName="campaign")]
	public class Campaign {
		[Key]
		public int Id { get; set; }
		[XmlElement(ElementName="imageData")]
		public ImageData ImageData { get; set; }

		[XmlElement(ElementName="name")]
		public string Name { get; set; }

		[XmlElement(ElementName="pc")]
		public List<Creature> PCs { get; set; }

		[XmlElement(ElementName="npc")]
		public List<Creature> Npcs { get; set; }

		[XmlElement(ElementName="adventure")]
		public List<Adventure> Adventures { get; set; }
		[XmlElement(ElementName="note")]
		public List<Note> Notes { get; set; }
	}

	[XmlRoot(ElementName="data")]
	public class Root {
		[XmlElement(ElementName="campaign")]
		public Campaign Campaign { get; set; }
		[XmlAttribute(AttributeName="version")]
		public string Version { get; set; }
	}

}
