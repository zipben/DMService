using System.Xml.Serialization;

namespace DataModels.Enums
{
    public enum CreatureCategory{
        PlayerCharacter = 1,
        NonPlayerCharacter = 2,
        Monster = 3
    }
    public enum EncounterState{
        
        [XmlEnum("0")]
        WaitingToRollInitiative = 0,
        [XmlEnum("1")]
        WaitingToStart = 1,
        [XmlEnum("2")]
        InProgress = 2,
        [XmlEnum("3")]
        Complete = 3
    }
    public enum SkillType{
        [XmlEnum("0")]
        Acrobatics = 0,
        [XmlEnum("1")]
        AnimalHandling = 1,
        [XmlEnum("2")]
        Arcana = 2,
        [XmlEnum("3")]
        Athletics = 3,
        [XmlEnum("4")]
        Deception = 4,
        [XmlEnum("5")]
        History = 5,
        [XmlEnum("6")]
        Insight = 6,
        [XmlEnum("7")]
        Intimidation = 7,
        [XmlEnum("8")]
        Investigation = 8,
        [XmlEnum("9")]
        Medicine = 9,
        [XmlEnum("10")]
        Nature = 10,
        [XmlEnum("11")]
        Perception = 11, 
        [XmlEnum("12")]
        Performance = 12,
        [XmlEnum("13")]
        Persuasion = 13,
        [XmlEnum("14")]
        Religion = 14,
        [XmlEnum("15")]
        SlightOfHand = 15,
        [XmlEnum("16")]
        Steath = 16,
        [XmlEnum("17")] 
        Survival = 17
        
    }    
}