using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models.CampaignModels;

namespace DAL.DataContext
{
    public class DataContext:DbContext
    {
        public DataContext(DbContextOptions<DataContext> options):base(options) 
        { 

        }

        public DbSet<Creature> Creatures {get;set;}
        public DbSet<SavingThrow> SavingThrows { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<Attack> Attacks { get; set; }
        public DbSet<Models.CampaignModels.Action> Actions { get; set; }
        public DbSet<Reaction> Reactions { get; set; }
        public DbSet<Combatant> Combatants { get; set; }
        public DbSet<Encounter> Encounters { get; set; }
        public DbSet<Trait> Traits { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Adventure> Adventures { get; set; }
        public DbSet<Spell> Spells { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }


    }
}