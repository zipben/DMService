using System.Collections.Generic;
using System.Linq;
using DAL.DataContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.CampaignModels;

namespace repos.Controllers 
{ 
    [ApiController]
    [Route("[controller]")]
    public class CampaignElementsController : ControllerBase {

        private readonly DataContext _context;
        public CampaignElementsController(DataContext context){
            _context = context;
        }

        [HttpGet("Encounters")]
        public List<Encounter> GetCollection(){
            return _context.Encounters.ToList();
        }

        [HttpGet("Encounters/{id}")]
        public Encounter GetEncounterDetails(int id){
            return _context.Encounters.Include(e => e.Combatant).ThenInclude(c => c.Creature).FirstOrDefault(e => e.Id == id);
        }

        [HttpGet("Campaigns")]
        public List<Campaign> GetCampaignCollection(){
            return _context.Campaigns.ToList();
        }

        [HttpGet("Campaigns/{id}")]
        public Campaign GetCampaignDetails(int id){
            return _context.Campaigns.Include(c => c.Adventures)
                                     .Include(c => c.Npcs)
                                     .Include(c => c.PCs)
                                     .Include(c => c.Notes).FirstOrDefault(e => e.Id == id);
        }

        [HttpGet("Campaigns/{id:int}/Adventures")]
        public List<Adventure> GetCampaignAdventures(int id){
             return _context.Campaigns.Include(c => c.Adventures).FirstOrDefault(c => c.Id == id).Adventures.ToList();
        }
    }
}