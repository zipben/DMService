﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DAL.DataContext;
using DataModels.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Models.CampaignModels;

namespace repos.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ImportController : ControllerBase
    {
        private readonly ILogger<ImportController> _logger;
        private readonly DataContext _dataContext;

        public ImportController(ILogger<ImportController> logger, DataContext dataContext)
        {
            _logger = logger;
            _dataContext = dataContext;
        }

        [HttpPost, DisableRequestSizeLimit]
        public IActionResult Upload(IFormFile file)
        {
            //Deserialize The incoming XML
            XmlSerializer serializer = new XmlSerializer(typeof(Root)); 
            Root root = serializer.Deserialize(file.OpenReadStream()) as Root;

            root.Campaign.PCs.ForEach(p => p.CreatureCategory = CreatureCategory.PlayerCharacter);
            root.Campaign.Npcs.ForEach(p => p.CreatureCategory = CreatureCategory.NonPlayerCharacter);

            //Get map of PCs to populate combatants in encounters
            Dictionary<string, Creature> pcUidMap = root.Campaign.PCs.ToDictionary(p =>p.Uid, p =>p);
            Dictionary<string, Creature> npcUidMap = root.Campaign.Npcs.ToDictionary(n =>n.Uid, n =>n);

            //The incoming files have "monster" tags inside the encounter entity but when 
            //the monster is actually a PC, it doesnt populate the values, instead it just references the UID
            //that came in with the pc, so this loops over all the encounters, and populates the combatant with the PC so
            //when we stick them in the 
            foreach(var adventure in root.Campaign.Adventures){
                foreach(var encounter in adventure.Encounter){
                    foreach(var combatant in encounter.Combatant){
                        Creature mappedCombatant = null;

                        if(combatant.Creature.Uid != null)
                        {
                            if(pcUidMap.TryGetValue(combatant.Creature.Uid, out mappedCombatant) || 
                               npcUidMap.TryGetValue(combatant.Creature.Uid, out mappedCombatant)){
                                
                                //if the creature is either an NPC or a PC, then we need to get it from the map
                                combatant.Creature = mappedCombatant;
                            }
                            else{
                                combatant.Creature.CreatureCategory = CreatureCategory.Monster;
                            }

                        }
                    }
                }
            }

            _dataContext.Campaigns.Add(root.Campaign);
            _dataContext.SaveChanges();

            return new OkResult();
        }

        [HttpGet]
        public Adventure Get(){
            return _dataContext.Adventures.Include(a => a.Encounter).ThenInclude(e => e.Combatant).ThenInclude(c => c.Creature).FirstOrDefault();
        }
    }
}
